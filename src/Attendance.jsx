import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import 'react-table-v6/react-table.css'


const Attendance = (props) => {
    const [att, setAtt] = useState([]);
    useEffect(() => {
        fetch("https://e1800927-semdemo.herokuapp.com/attendances", {
            method: "GET",
            headers: {
                Accept: "application/json",
                Authorization: "Basic " + btoa("user:password123")
            }
        }).then((response) => {
            console.log(response);
            response.json().then((data) => {
                setAtt(data);
            });
        })
    }, []);


    var columns = [
        {
            Header: "Id",
            accessor: "id",
        },
        {
            Header: "Key",
            accessor: "key",
        },
    ];


    return (
        <div>
            <h1>{props.title}</h1>
            <ReactTable data={att} columns={columns} />
        </div>
    );
}

export default Attendance;